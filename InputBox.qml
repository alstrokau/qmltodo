import QtQuick 2.0

Rectangle {
    id: root
    property alias text: itInput.text
    signal xx

    color: itInput.activeFocus ? "lightgray" : Qt.darker("lightgray")
    border.width: itInput.activeFocus ? 2 : 1
    height: parent.height * 0.8


    TextInput{
        id: itInput
        anchors.fill: parent
        anchors.margins: 10
        clip: true
        font.pixelSize: parent.height * 0.7

        onTextChanged: {
            root.xx();
        }

        inputMethodHints: Qt.ImhNoPredictiveText
    }
}

