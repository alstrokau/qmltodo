import QtQuick 2.0

Rectangle {
    property alias text: label.text
    signal act

    id: root

    width: 100

    height: parent.height * 0.8
    anchors.verticalCenter: parent.verticalCenter

    border.color: maButt.pressed ? Qt.lighter(color) : Qt.darker(color)
    border.width: height * 0.08

    MouseArea{
        id: maButt
        anchors.fill: parent
        onClicked: {
            root.act();
        }
    }

    Text {
        id: label
        anchors.centerIn: parent
        font.pixelSize: parent.height * 0.8
        color: Qt.darker(Qt.darker(parent.color))
    }
}

