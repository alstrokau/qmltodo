#include <QFile>
#include <QTextStream>
#include <QDebug>

#include "todomodel.h"

TodoModel::TodoModel(QObject *parent) : QObject(parent)
{
    loadList();
}

QStringList TodoModel::list()
{
    return _list;
}

void TodoModel::setList(QStringList &list)
{
    if(list != _list){
        _list = list;
        processModelChange();
    }
}

void TodoModel::addItem(const QString &item)
{
    if(item.isEmpty()){
        return;
    }

    _list.append(item);
    processModelChange();
}

void TodoModel::rmItem(const int index)
{
    if(index >= 0 && index < _list.count()){
        _list.removeAt(index);
        processModelChange();
    }
}

void TodoModel::saveList() const
{
    QString path;
    QString fName = "todo.list";

    path = findPathToStoreFile();
    if(path.isEmpty()){
        qDebug() << "cannot find sdcard, aborting";
        return;
    }
    fName = path + "/" + fName;

    qDebug() << QString("attempt to save list to file")
                .arg(fName);

    QFile f(fName);
    if(f.open(QIODevice::WriteOnly)){
        QTextStream stream(&f);

        QString out;
        out = _list.join(';');
        stream << out;

        f.close();
    }

    qDebug() << "write list to file done";
}

void TodoModel::loadList()
{
    QString path;
    QString fName = "todo.list";

    path = findPathToStoreFile();
    if(path.isEmpty()){
        qDebug() << "cannot find sdcard, aborting";
        return;
    }
    fName = path + "/" + fName;

    qDebug() << QString("attempt to read list from file")
                .arg(fName);

    QFile f(fName);
    if(f.open(QIODevice::ReadOnly)){
        QTextStream stream(&f);

        QString in;
        stream >> in;

        _list = in.split(';');

        f.close();
    }

    qDebug() << "read list from file done";

    emit modelChanged();
}

void TodoModel::processModelChange()
{
    saveList();
    emit modelChanged();
}


