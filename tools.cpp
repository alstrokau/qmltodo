#include <QString>
#include <QDir>

QString findPathToStoreFile()
{
    QStringList possiblePaths;
    possiblePaths << "s:/"
                  << "d:/"
                  << "c:/"
                  << "/mnt/sdcard"
                  << "/mnt/sdcard/ext_sd"
                  << "/mnt/extSdCard"
                  << "/storage/external_sd"
                  << "/storage/sdcard1"
                  << "/storage/sdcard0"
                  << "/storage/sdcard"
                  << "/storage/extSdCard";


    foreach (QString path, possiblePaths) {
        QDir d(path);
        if(d.exists()){
            return path;
        }
    }

    return "";
}
