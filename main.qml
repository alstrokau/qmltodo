import QtQuick 2.4
import QtQuick.Window 2.2



Window {
    visible: true
    width: 480
    height: 800

    function checkText(txt){
        btnAdd.color = txt === ""? "gray" : "lightgreen"
    }

    Rectangle{
        id: rBg
        anchors.fill: parent
        anchors.margins: 5
        color: "whitesmoke"
        border{width: 1; color: Qt.darker(color)}

        Rectangle{
            id: rView
            color: "whitesmoke"
            border{width: 1; color: Qt.darker(color)}
            anchors{
                margins: 5
                left: parent.left
                right: parent.right
                bottom: rCtrl.top
                top: parent.top
            }

            ListView{
                id: lvViewver
                anchors.fill: parent

                model: todomodel.model

                delegate:
                    TodoItemDelegate{
                        id: currDelegate
                        text: modelData
                        textNum: index + 1
                        isCurr: lvViewver.currentIndex == index;

                        onAct: {
//                            console.log("item act 2");
                            lvViewver.currentIndex = index;
                        }
                        onRemove: {
//                            console.log("rm item");
                            todomodel.rmItem(index);
                        }
                    }
                add: Transition { NumberAnimation { properties: "y"; from: rBg.height; duration: 250 } }
                remove: Transition { NumberAnimation { property: "opacity"; to: 0; duration: 1500 } }
            }
        }

        Rectangle{
            id: rCtrl
            color: "whitesmoke"
            border{width: 1; color: Qt.darker(color)}
            anchors{
                margins: 5
                left: parent.left
                right: parent.right
                bottom: parent.bottom
            }
            height: parent.height / 8


            InputBox{
                id: ibNewItem
                anchors{
                    margins: 10
                    left: parent.left
                    right: btnAdd.left
                    verticalCenter: parent.verticalCenter
                }
                onXx: {
                    checkText(ibNewItem.text);
                }
            }

            Butt{
                id: btnAdd
                anchors{
                    right: parent.right
                    margins: 10
                }
                color: "gray"
                height: parent.height * 0.8

                text: "+"

                onAct: {
                    todomodel.addItem(ibNewItem.text);
                    console.log("add new word [" + ibNewItem.text + "]");
                    ibNewItem.text = "";
                    lvViewver.currentIndex = -1
                    Qt.inputMethod.hide();
                }
            }
        }
    }
}
