#ifndef TODOMODEL_H
#define TODOMODEL_H

#include <QObject>
#include "tools.h"

class TodoModel : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QStringList model READ list WRITE setList NOTIFY modelChanged)
public:
    explicit TodoModel(QObject *parent = 0);

    QStringList list();
    void setList(QStringList& list);

    Q_INVOKABLE void addItem(const QString& item);
    Q_INVOKABLE void rmItem(const int index);
signals:
    void modelChanged();
public slots:

private:
    QStringList _list;

    void saveList() const;
    void loadList();
    void processModelChange();
};

#endif // TODOMODEL_H
