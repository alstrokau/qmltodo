import QtQuick 2.0

Item{
    id: itRoot
    signal remove

    property alias text: label.text
    property alias textNum: lbNum.text
    property bool isCurr: false
    signal act

    height: 50
    anchors.left: parent.left
    anchors.right: parent.right

    MouseArea{
        id: maItem
        anchors.fill: parent
        onClicked: {
            itRoot.act();
        }
    }

    Rectangle{
        id: rNum
        height: itRoot.height
        width: height * 1.1
        color: "darkgray"
        border.width: isCurr ? 3 : 1
        radius: 5

        anchors{
            verticalCenter: parent.verticalCenter
            left: parent.left
        }

        Text {
            id: lbNum
            anchors.centerIn: parent
            font.pixelSize: parent.height * 0.8
            font.bold: isCurr ? true : false
        }
    }

    Rectangle{
        id: rData
        height: itRoot.height
        color: "silver"

        border.width: isCurr ? 2 : 1
//        radius: 5
        anchors.left: rNum.right
        anchors.right: parent.right

        Text{
            id: label
            text: modelData
            anchors.fill: parent
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.leftMargin: 10

            font.pixelSize: parent.height * 0.8
        }
    }

    Butt{
        id: btnRemove
        visible: isCurr

        anchors{
            right: parent.right
        }
        width: height
        height: parent.height
        text: "-"
        color: "red"

        onAct: {
//            console.log("attemt to remove item");
            itRoot.remove()
        }
    }
}

