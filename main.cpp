#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include "todomodel.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    TodoModel tm;

    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("todomodel", &tm);
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}
